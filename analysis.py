import pandas as pd
import matplotlib.pyplot as plt

pd.set_option('display.max_columns', 8)

# Import data from 3 CSV files
general = pd.read_csv('test/general.csv')
prenatal = pd.read_csv('test/prenatal.csv')
sports = pd.read_csv('test/sports.csv')

# Make all the column names match
prenatal.columns = general.columns
sports.columns = general.columns

# Merge all the dataframes together
df = pd.concat([general, prenatal, sports], ignore_index=True)

# Drop the old index column
df.drop(columns='Unnamed: 0', inplace=True)

# Delete all the empty rows
df.dropna(how='all', inplace=True)

# Correct all the gender column values to f and m respectively
df.loc[df['gender'] == 'male', 'gender'] = 'm'
df.loc[df['gender'] == 'man', 'gender'] = 'm'
df.loc[df['gender'] == 'female', 'gender'] = 'f'
df.loc[df['gender'] == 'woman', 'gender'] = 'f'

# Replace the NaN values in the gender column with f
df['gender'].fillna('f', inplace=True)

# Replace the NaN values in the stats columns with 0
df['bmi'].fillna(0, inplace=True)
df['diagnosis'].fillna(0, inplace=True)
df['blood_test'].fillna(0, inplace=True)
df['ecg'].fillna(0, inplace=True)
df['ultrasound'].fillna(0, inplace=True)
df['mri'].fillna(0, inplace=True)
df['xray'].fillna(0, inplace=True)
df['children'].fillna(0, inplace=True)
df['months'].fillna(0, inplace=True)


# Which hospital has the highest number of patients?
ans_1 = df['hospital'].value_counts().idxmax()
print(f'Which hospital has the highest number of patients? {ans_1}')

# What share of the patients in the general hospital suffer from stomach-related issues?
ans_2 = (df[(df['hospital'] == 'general') & (df['diagnosis'] == 'stomach')]['diagnosis'].count()
         / df[(df['hospital'] == 'general')]['diagnosis'].count()).round(3)
print(f'What share of the patients in the general hospital suffer from stomach-related issues? {ans_2}')

# What share of the patients in the sports hospital suffer from dislocation issues?
ans_3 = (df[(df['hospital'] == 'sports') & (df['diagnosis'] == 'dislocation')]['diagnosis'].count()
         / df[(df['hospital'] == 'sports')]['diagnosis'].count()).round(3)
print(f'What share of the patients in the sports hospital suffer from dislocation issues? {ans_3}')

# What is the difference in the median ages of the patients in the general and sports hospital?
ans_4 = round(df.groupby('hospital')['age'].median()['general'] - df.groupby('hospital')['age'].median()['sports'])
print(f'What is the difference in the median ages of the patients in the general and sports hospital? {ans_4}')

# Which hospital took the most blood tests and how many were taken?
ans_5_pt1 = df[df['blood_test'] == 't']['hospital'].value_counts().idxmax()
ans_5_pt2 = df[df['blood_test'] == 't']['hospital'].value_counts().max()
print(f'Which hospital took the most blood tests and how many were taken? {ans_5_pt1}, {ans_5_pt2} blood tests')


general_df = df[df['hospital'] == 'general']
prenatal_df = df[df['hospital'] == 'prenatal']
sports_df = df[df['hospital'] == 'sports']

# What is the most common age of a patient among all hospitals?
bins = [0, 15, 35, 55, 70, 80]
age_df = [general_df['age'], prenatal_df['age'], sports_df['age']]
plt.hist(age_df, bins=bins, stacked=True, label=['General', 'Prenatal', 'Sports'])
plt.legend()
plt.title('What is the most common age of a patient among all hospitals?')
plt.show()
print('What is the most common age of a patient among all hospitals? 15-35')

# What is the most common diagnosis among patients in all hospitals?
plt.pie(df['diagnosis'].value_counts(),
        labels=df['diagnosis'].value_counts().index.tolist())
plt.title('What is the most common diagnosis among patients in all hospitals?')
plt.show()
print('What is the most common diagnosis among patients in all hospitals? pregnancy')

# Build a violin plot of height distribution by hospitals
f, (ax1, ax2) = plt.subplots(1, 2, sharey='all')
f.suptitle('Height distribution by hospital type')
f.subplots_adjust(wspace=0)
ax1.set_ylabel("Height distribution")

ax1.violinplot(df['height'])
ax1.set_xticks([1])
ax1.set_xticklabels(["All Hospitals"])

height_df = [general_df['height'], prenatal_df['height'], sports_df['height']]
ax2.violinplot(height_df)
ax2.set_xticks((1, 2, 3))
ax2.set_xticklabels(("General", "Prenatal", "Sports"))

plt.show()
print('Build a violin plot of height distribution by hospitals: '
      'Sports hospitals have a higher distribution than general '
      'or prenatal hospitals. This results in two peaks in the '
      'combined violin chart')
